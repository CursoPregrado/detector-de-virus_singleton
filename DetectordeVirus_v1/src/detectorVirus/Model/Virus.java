/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package detectorVirus.Model;

/**
 *
 * @author data
 */
public class Virus {
    
    private String nombreVirus;
    private byte [] secuenciaVirus;

    public Virus(String nombreVirus, byte[] secuenciaVirus) {
        this.nombreVirus = nombreVirus;
        this.secuenciaVirus = secuenciaVirus;
    }

    public String getNombreVirus() {
        return nombreVirus;
    }

    public byte[] getSecuenciaVirus() {
        return secuenciaVirus;
    }
}
