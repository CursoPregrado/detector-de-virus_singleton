/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package detectorVirus.Controllers;

import detectorVirus.Model.Virus;

/**
 *
 * @author data
 */
public class Analizador {
    
    private Virus [] listaVirus = new Virus [3];
    private byte [] contenidoArchivo;

    public Analizador(byte[] contenidoArchivo) {
        
        this.contenidoArchivo = contenidoArchivo;
    }
    
    private void cargarVirus (){
        
        byte [] usama = { 13, 10, 13, 10};
        byte [] amtrax = { 105, 108, 100, 13}; 
        byte [] ebola = { 29, 32, 53, 29}; 
    
        listaVirus [0] = new Virus ("Usama", usama);
        listaVirus [1] = new Virus ("Amtrax", amtrax);
        listaVirus [2] = new Virus ("éBola", ebola);
    }
    
    public void analizarArchivo(){
    
        cargarVirus();
        
        for (int i =0; i < contenidoArchivo.length; i++){
        
            //USAMA
            if (contenidoArchivo [i] == listaVirus[0].getSecuenciaVirus()[0] && i<(contenidoArchivo.length-3)){
            
                if (contenidoArchivo [i+1] == listaVirus[0].getSecuenciaVirus()[1]){
            
                    if (contenidoArchivo [i+2] == listaVirus[0].getSecuenciaVirus()[2]){
                        
                        if (contenidoArchivo [i+3] == listaVirus[0].getSecuenciaVirus()[3]){
            
                            System.out.println("USAMA encontrado");
            
                        }
                    }
                }
            }
            
            //Amtrax
            if (contenidoArchivo [i] == listaVirus[1].getSecuenciaVirus()[0] && i<(contenidoArchivo.length-3)){
            
                if (contenidoArchivo [i+1] == listaVirus[1].getSecuenciaVirus()[1]){
            
                    if (contenidoArchivo [i+2] == listaVirus[1].getSecuenciaVirus()[2]){
                        
                        if (contenidoArchivo [i+3] == listaVirus[1].getSecuenciaVirus()[3]){
            
                            System.out.println("Amtrax encontrado");
            
                        }
                    }
                }
            }
            
            //Ebola
            if (contenidoArchivo [i] == listaVirus[2].getSecuenciaVirus()[0] && i<(contenidoArchivo.length-3)){
            
                if (contenidoArchivo [i+1] == listaVirus[2].getSecuenciaVirus()[1]){
            
                    if (contenidoArchivo [i+2] == listaVirus[2].getSecuenciaVirus()[2]){
                        
                        if (contenidoArchivo [i+3] == listaVirus[2].getSecuenciaVirus()[3]){
            
                            System.out.println("Ebola encontrado");
            
                        }
                    }
                }
            }
        }
    }
    
}
