/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package detectorVirus.Controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 *
 * @author data
 */
public class Admin_Archivos {
    
    private byte [] archivoLeido;
    private static Admin_Archivos adminSingleton;
    
    private Admin_Archivos(){
        
        System.out.println("Ingreso Singeton");
    }
    
    public static Admin_Archivos getInstanciaSingleton(){
    
        if(adminSingleton == null){
            
            adminSingleton = new Admin_Archivos();
        
        }
    
        return adminSingleton;
    }
    
    public void readFile (File archivo) throws IOException{
        
         archivoLeido = Files.readAllBytes(archivo.toPath());
    }

    public byte[] getArchivoLeido() {
        return archivoLeido;
    }
    
}
